environment = "qa"
thresholds = 
{
    acu_threshold = 90,
    cpu_threshold = 90
}
subscription_endpoint = "qa-cfa@phdata.io"