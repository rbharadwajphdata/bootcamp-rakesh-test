variable "environment" {
  description = "Environment name"
}

variable "tags" {
  description = "Tags to apply to resources created by the module"
  type        = map(string)
  default     = {}
}

