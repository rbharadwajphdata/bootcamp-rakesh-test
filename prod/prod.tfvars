environment = "prod"
alarm_thresholds = 
{
    acu_threshold = 90,
    cpu_threshold = 90
}
subscription_endpoint = "prod-cfa@phdata.io"