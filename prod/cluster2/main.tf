# SNS topic
module "sns_topic" {
  source     = "../modules/sns_topic"
  topic_name = "${var.cluster_name}-${var.environment}-monitoring-topic"
}

# Cloudwatch alarms
module "cloudwatch_alarms" {
  source = "../modules/cloudwatch_alarms"

  cluster_name = var.cluster_name
  environment  = var.environment
  tags = var.tags
}

#RDS events
module "rds_events" {
  source = "../modules/rds_events"

  cluster_name   = var.cluster_name
  environment    = var.environment
  sns_topic_name = "rds-events-${var.cluster_name}-${var.environment}"
  tags           = var.tags
}
